﻿# DECLARATION

#Cesta ke složce
$Path = 'C:\Users\petrs\Desktop\Script'

#Hledané soubory
$Filter = '*.csv'

#Konfigurace pošty
$From = "Server Reporter <kao.kao@seznam.cz>"
$To = "Petr Štěpán <pstepan@totalservice.cz>"
$Subject = "Files Report $(Get-Date -Format 'dd.M.yyyy')"
$SMTPServer = "smtp.seznam.cz"
$SMTPPort = "587"
$Username = "kao.kao@seznam.cz"



##########################################################################
#                         Začátek skriptu                                #
##########################################################################

#Cesta k uložení hesla
$SecPassPath = "$PSScriptRoot\Pass.securestrig"

$style = "<style>BODY{font-family: Arial; font-size: 10pt;}"
$style = $style + "H1{font-size: 15pt;}"
$style = $style + "EM{font-size: 8pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"

$Title = "<h1> CSV Report $(Get-Date -Format 'dd.M.yyyy') </h1>"


$Body = Get-ChildItem -Path $Path -Filter $Filter | select Name, @{Label='Size (kB)';E={[math]::Round(($_.Length/1kB),2)}}, @{Label='Modified';E={$_.LastWriteTime}} | ConvertTo-Html -Head $style -Body $Title


#Nacti heslo
If(Test-Path "$SecPassPath" -PathType Leaf){
    $securePwd = Get-Content -Path "$SecPassPath" | ConvertTo-SecureString    
}
else{
    $r = Get-Credential -UserName $Username -Message "Login to SMTP server"
    $securePwd = $r.Password
    $r.Password | ConvertFrom-SecureString | Out-File -FilePath "$SecPassPath"
}

#Prihlasovaci udaje
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $Username, $securePwd
#Odesli email
Send-MailMessage -From $From -to $To -Subject $Subject -BodyAsHtml $Body -SmtpServer $SMTPServer -port $SMTPPort -Credential $cred -UseSsl -Encoding utf8
