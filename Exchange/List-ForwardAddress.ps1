﻿#requires -version 3

<#
.SYNOPSIS
  List mailboxes where forwarding address parametr is set up.

.DESCRIPTION
  List mailboxes where forwarding address parametr is set up. In case that forward address is distribution group, you can use parameter -ExpandGroups to display all members of group.

.NOTES
  Version:        1.0
  Author:         Petr Štěpán
  Email:          pstepan@totalservice.cz
  Creation Date:  22.10.2018
  Purpose/Change: Initial script development

.EXAMPLE
  List mailboxes with forward address parametr
  
  .\List-ForwardAddress.ps1

.EXAMPLE
  List mailboxes with forward address parametr as table
  
  .\List-ForwardAddress.ps1 | Format-Table

.EXAMPLE
  List mailboxes with forward address parametr and expand members of distribution groups
  
  .\List-ForwardAddress.ps1 -ExpandGroups

.EXAMPLE
  Export mailboxes with forward address to csv
  
  .\List-ForwardAddress.ps1 -ExpandGroups | Export-Csv <path>\<filename>.csv -Encoding UTF8

#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

[CmdletBinding()]
Param (
  #Script parameters go here
  [Parameter(Position=1)]
  [switch]$ExpandGroups
  
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------
# Check Exchange Management Shell, attempt to load 
if (!(Get-Command Get-ExchangeServer -ErrorAction SilentlyContinue)) 
{ 
    
    if (Test-Path "C:\Program Files\Microsoft\Exchange Server\V15\bin\RemoteExchange.ps1") #EX2013
    { 
        . 'C:\Program Files\Microsoft\Exchange Server\V15\bin\RemoteExchange.ps1' 
        Connect-ExchangeServer -auto 
    } elseif (Test-Path "C:\Program Files\Microsoft\Exchange Server\V14\bin\RemoteExchange.ps1"){ 
        . 'C:\Program Files\Microsoft\Exchange Server\V14\bin\RemoteExchange.ps1' 
        Connect-ExchangeServer -auto 
    } elseif (Test-Path "C:\Program Files\Microsoft\Exchange Server\bin\Exchange.ps1") { 
        Add-PSSnapIn Microsoft.Exchange.Management.PowerShell.Admin 
        .'C:\Program Files\Microsoft\Exchange Server\bin\Exchange.ps1' 
    } else { 
        throw "Exchange Management Shell cannot be loaded" 
    } 
}

#-----------------------------------------------------------[Functions]------------------------------------------------------------
#Return email address
function _getForwardEmail
{
    PARAM(
        [Parameter(Mandatory=$true, Position=1)]
        $ForwardingAddress

    )

    return (Get-ADObject -Identity $(($ForwardingAddress).DistinguishedName) -Properties mail).mail
}

#Return type user or group
function _getForwardType
{
    PARAM(
        [Parameter(Mandatory=$true, Position=1)]
        $ForwardingAddress
    )

    return (Get-ADObject -Identity $(($ForwardingAddress).DistinguishedName) -Properties ObjectClass).ObjectClass
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------
#List all mailboces with ForwardAddress parameter
$mb = Get-Mailbox | where {$_.ForwardingAddress -ne $null}

$result=@()
foreach($m in $mb)
{

    $obj = New-Object -TypeName PSObject
	$obj | Add-Member -MemberType NoteProperty -Name Name -Value $m.Name
	$obj | Add-Member -MemberType NoteProperty -Name Username -Value $m.Alias
	$obj | Add-Member -MemberType NoteProperty -Name ForwardingAddress -value (_getForwardEmail $m.ForwardingAddress)
	$obj | Add-Member -MemberType NoteProperty -Name ForwardingType -value (_getForwardType $m.ForwardingAddress)

    if($ExpandGroups)
    {
        if((_getForwardType $m.ForwardingAddress) -eq "group")
        {
            $members = $members = '{' + ((Get-DistributionGroupMember $m.ForwardingAddress).PrimarySMTPAddress -join '; ') + '}'
            $obj | Add-Member -MemberType NoteProperty -Name GroupMembers -value $members
        }else
        {
            $obj | Add-Member -MemberType NoteProperty -Name GroupMembers -value "{}"
        }
    }    


    $obj | Add-Member -MemberType NoteProperty -Name DeliveryToBoth -value $m.DeliverToMailboxAndForward
			
	$result += $obj
}

$result | Sort-Object -Property Name

<#
#If ExpandGrops is set
if($ExpandGroups)
{
    foreach($m in $mb)
    {
        if($m.ForwardingType -eq "group")
        {
            $members = '{' + ((Get-DistributionGroupMember $m.ForwardingAddress).PrimarySMTPAddress -join '; ') + '}'
            $m | Add-Member -MemberType NoteProperty -Name GroupeMembers -Value $members -PassThru
        }
    }
}

$mb = Get-Mailbox | where {$_.ForwardingAddress -ne $null} |Sort-Object -Property Name | select Name, @{l='Username'; e={$_.Alias}}, @{l='ForwardingAddress'; e={(_getForwardEmail $_.ForwardingAddress)}}, @{l='ForwardingType'; e={(_getForwardType $_.ForwardingAddress)}}, @{l='DeliveryToBoth'; e={$_.DeliverToMailboxAndForward}}

#>