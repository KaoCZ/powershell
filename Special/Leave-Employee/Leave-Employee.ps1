﻿#requires -version 3

<#
.SYNOPSIS
  This script makes leave employee procedure easier and faster. After run script, the user will be disabled in AD and removed from all distributions groups. Optionaly you can setup email forwarding or Out of Office replay.

.DESCRIPTION
  The Leave-Employee script makes leave employee procedure easier and faster.

  The script requires one parametr -Username. It's the username of person who leave company. After run the script user account is disabled and remove from all distributions grops.

  Optionaly you can setup email forwarding, by default emails are delivered to both mailboxes or setup Out of Office autoreplay whit your prefered text.

  Template for Out of Office autoreplay is loded from external file. In template you can use 3 keywords _NAME_, _FORWARD_NAME_, _FORWARD_EMAIL_ (keywords can be changed).

  _NAME_ - replaced by full name leaving person loaded from AD
  _FORWARD_EMAIL_ - replaced with the email of the person specified in the parameter -OoOEmail
  _FORWARD_NAME_ - replaced with the full name of the person specified in the parameter -OoOEmail

.NOTES
  Version:        1.0
  Author:         Petr Štěpán
  Email:          pstepan@totalservice.cz
  Creation Date:  16.10.2018
  Purpose/Change: Initial script development

.EXAMPLE
  Disable in AD and remove user from all distribution groups.
  
  Leave-Employee -Username <username>

.EXAMPLE
  Disable in AD and remove user from all distribution groups. Setup forwarding emails.
  
  Leave-Employee -Username <username> -ForwardingAddress <username>

.EXAMPLE
  Disable in AD and remove user from all distribution groups. Setup Out of Office message.
  
  Leave-Employee -Username <username> -OoOTemplatePath C:\Path_to_file\Template.html

.EXAMPLE
  Disable in AD and remove user from all distribution groups. Setup Out of Office message and use specificated email in template.
  
  Leave-Employee -Username <username> -OoOTemplatePath C:\Path_to_file\Template.html -OoOEmail <username> or <vaild email address>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------
[CmdletBinding()]
Param (
  #Script parameters go here
  [Parameter(Mandatory=$True,Position=1, HelpMessage="Please enter the username of leaving employee")]
  [string]$Username,
  
  [Parameter(Position=2, HelpMessage="Email address where emails will be forwarded")]
  [string]$ForwardingAddress,

  [Parameter(Position=3, HelpMessage="Input path to Out Of Office template")]
  [string]$OoOTemplatePath,

  [Parameter(Position=4, HelpMessage="Input email address which will be mentioned in template text")]
  [string]$OoOEmail
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
#$ErrorActionPreference = 'SilentlyContinue'

#Variables
##Email templates keywords
$OoOTemplate = [PSCustomObject]@{
    Keyword_EmployeeName = "_NAME_"
    Keyword_ForwardName = "_FORWARD_NAME_"
    Keyword_ForwardEmail = "_FORWARD_EMAIL_"
    Path = ""
    Text = ""
    TemplateForwardName = ""
    TemplateForwardEmail = ""
}

#Reset variable
$userIdentity=''

#Import Modules & Snap-ins

# Check Exchange Management Shell, attempt to load 
if (!(Get-Command Get-ExchangeServer -ErrorAction SilentlyContinue)) 
{ 
    
    if (Test-Path "C:\Program Files\Microsoft\Exchange Server\V15\bin\RemoteExchange.ps1") #EX2013
    { 
        . 'C:\Program Files\Microsoft\Exchange Server\V15\bin\RemoteExchange.ps1' 
        Connect-ExchangeServer -auto 
    } elseif (Test-Path "C:\Program Files\Microsoft\Exchange Server\V14\bin\RemoteExchange.ps1"){ 
        . 'C:\Program Files\Microsoft\Exchange Server\V14\bin\RemoteExchange.ps1' 
        Connect-ExchangeServer -auto 
    } elseif (Test-Path "C:\Program Files\Microsoft\Exchange Server\bin\Exchange.ps1") { 
        Add-PSSnapIn Microsoft.Exchange.Management.PowerShell.Admin 
        .'C:\Program Files\Microsoft\Exchange Server\bin\Exchange.ps1' 
    } else { 
        throw "Exchange Management Shell cannot be loaded" 
    } 
}

#Validate Forwarding email address
if($ForwardingAddress)
{
    if(!(Get-Mailbox $ForwardingAddress -ErrorAction SilentlyContinue))
    {
        Write-Error -Message "Forwarding email address $ForwardingAddress doesn't exist!" -ErrorAction Stop
    }
}

#Validate Out of Office email address
if($OoOEmail)
{
    if(!($OoOEmail = Get-Mailbox $OoOEmail -ErrorAction SilentlyContinue | Select -ExpandProperty PrimarySMTPAddress))
    {
        Write-Error -Message "Email address $OoOEmail for Out of Office doesn't exist!" -ErrorAction Stop
    }
}

#Validate template config
if($OoOTemplatePath)
{ 
        #Test path
        if(!(Test-Path $OoOTemplatePath))
        {
            throw "Bad path to Out of Office template or file doesn't exist."
            
        }
        $OoOTemplate.Path = $OoOTemplatePath

        #Read template
        $OoOTemplate.Text = (Get-Content $OoOTemplate.Path) -join "`n"

        #Find template keywords
        if((Select-String -InputObject $OoOTemplate.Text -Pattern ('\b'+$OoOTemplate.Keyword_ForwardName+'\b')) -or (Select-String -InputObject $OoOTemplate.Text -Pattern ('\b'+$OoOTemplate.Keyword_ForwardEmail+'\b')))
        {         
            #Template include keywords
            if(!($OoOEmail))
            {
                throw "If template include keywords ($($OoOTemplate.Keyword_ForwardName) or $($OoOTemplate.Keyword_ForwardEmail)), parametr -OoOEmail must be use."
            }
        }
}

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here

#List of admin's groups. Member of this group will not be disabled.
$adminGroups = @("Enterprise Admins","Schema Admins","Domain Admins","Cert Publishers","Administrators","Account Operators")

#-----------------------------------------------------------[Functions]------------------------------------------------------------

#Verify insert user
function _getUser
{
    param(
        [Parameter(Mandatory=$true, Position=1)]
        [string]$Username
    )
    

    do{
        $userIdentity=''
        
        try
        {
            #$userIdentity = Get-ADUser $name | select -ExpandProperty SamAccountName
            [PSObject]$userIdentity = Get-ADUser $username -Properties Description | select SamAccountName, Name, Enabled, Description
        }catch{
            Write-Warning -Message "Oops... User doesn't exist..."
            $username = Read-Host "Enter the username again"
        }
        

    }until($userIdentity)

    return $userIdentity
}

#Check if user is member of administrator's groups
function _isAdmin
{
    param($username)
    $usersGroups = Get-ADPrincipalGroupMembership $username | ? {$_.GroupCategory -eq "Security"} | Select name

    if($usersGroups | Where {$adminGroups -Contains $_.Name})
    {
        return $true
    }else{
        return $false
    }
  
}

function _writeMessage
{
    Param(
        [Parameter(Mandatory=$True,Position=1)]
        [string]$message,
	
        [Parameter()]
        [switch]$ok
    )

    Write-Host $message " ..... " -NoNewline
    if($ok)
    {
        Write-Host "OK" -ForegroundColor Green
    }else{
        Write-Host "FAILED" -ForegroundColor Red
    }
    
}

function _getNameFromEmail
{
    Param(
        [Parameter(Mandatory=$True)]
        [string]$email
    )

    $ForwardUser = Get-Mailbox pepa.testovac@kajima.cz | select Displayname
    $ForwardUser += @(Email = $email)
}

<#
#Find user by inserting part of name
function _findUser
{
    param($name)
    $x = @(get-aduser -f {name -like '*Pepa Testovač*'} | select name)
    $x
}
#>

#-----------------------------------------------------------[Execution]------------------------------------------------------------
 
#Get input from user
cls
Write-Host "Welcome in leaving employee procedure."
           "**************************************"
           ""

$userIdentity = _getUser $username
Write-Host "User: "  -NoNewline
Write-Host $userIdentity.Name -ForegroundColor Green

<#
#Find username
$username = _findUser($name)
#>


#Check membership in Admin groups
$userIdentity | Add-Member -NotePropertyName isAdmin -NotePropertyValue (_isAdmin($userIdentity.SamAccountName))

if ($userIdentity.isAdmin)
{
    Write-Error "Administrators can't be managed by this script!" -ErrorAction Stop
}else{
    _writeMessage "User is not member of administrator's groups" -ok
}

#Disable account and add date of blocking to description
if($userIdentity.Enabled -eq $true)
{
    $userIdentity.SamAccountName | Disable-ADAccount
    $Description = (Get-Date -Format "dd.MM.yyyy") + " disabled by PS - " + $userIdentity.Description
    Set-ADUser $userIdentity.SamAccountName -Description $Description
}
Write-Host ""
_writeMessage "Disabling user" -ok

#Forward email address
if($ForwardingAddress)
{
    Set-Mailbox $userIdentity.SamAccountName -ForwardingAddress $ForwardingAddress -DeliverToMailboxAndForward $true
    Write-Host ""
    _writeMessage "Setup email forwarding to address $ForwardingAddress" -ok
}

#Remove from Distribution groups
$DistributionsGroups = Get-ADPrincipalGroupMembership $userIdentity.SamAccountName | ? {$_.GroupCategory -eq "Distribution"}
Write-Host ""
Write-Host "Removing user from distributions groups ..."
if(@($DistributionsGroups).Count -gt 0)
{

    foreach($Dg in $DistributionsGroups)
    {
        try
        {
            Remove-DistributionGroupMember $Dg.name -Member $userIdentity.SamAccountName -ErrorAction stop -confirm:$false
            _writeMessage "Remove $($userIdentity.Name) from $($Dg.Name)" -ok
        }catch{
            _writeMessage "Remove $($userIdentity.Name) from $($Dg.Name)"
        }
    }
}else
{
    _writeMessage "Remove $($userIdentity.Name) from distribution groups (user is not member of any distribution group)" -ok 
}

#Set up Out Of Office
if($OoOTemplatePath)
{
  Write-Host "Starting pripering Out of Office template ..."
  #If template include keywords, prepare text
  if($OoOEmail)
  {
    #Replace variable $OoOTemplate.Keyword_EmployeeName
    if(Select-String -InputObject $OoOTemplate.Text -Pattern ('\b'+$OoOTemplate.Keyword_EmployeeName+'\b'))
    {
       $OoOTemplate.Text = ($OoOTemplate.Text) -replace (('\b'+$OoOTemplate.Keyword_EmployeeName+'\b'), $userIdentity.Name)
       _writeMessage "Replace $($OoOTemplate.Keyword_EmployeeName) template variable" -ok 
    }
    
    #Replace variable $OoOTemplate.Keyword_ForwardName
    if(Select-String -InputObject $OoOTemplate.Text -Pattern ('\b'+$OoOTemplate.Keyword_ForwardName+'\b'))
    {
       $ForwardName = Get-Mailbox $OoOEmail | select -ExpandProperty DisplayName
       $OoOTemplate.Text = ($OoOTemplate.Text) -replace (('\b'+$OoOTemplate.Keyword_ForwardName+'\b'), $ForwardName)
       _writeMessage "Replace $($OoOTemplate.Keyword_ForwardName) template variable" -ok  
    }

    #Replace variable $OoOTemplate.Keyword_ForwardEmail
    if(Select-String -InputObject $OoOTemplate.Text -Pattern ('\b'+$OoOTemplate.Keyword_ForwardEmail+'\b'))
    {
       $OoOTemplate.Text = ($OoOTemplate.Text) -replace (('\b'+$OoOTemplate.Keyword_ForwardEmail+'\b'), $OoOEmail)
       _writeMessage "Replace $($OoOTemplate.Keyword_ForwardEmail) template variable" -ok 
    }
    
    #Setup Out of Office
    Set-MailboxAutoReplyConfiguration $userIdentity.SamAccountName -AutoReplyState enabled -ExternalAudience all -InternalMessage $OoOTemplate.Text -ExternalMessage $OoOTemplate.Text
    _writeMessage "Setting out of Office" -ok
  }else
  {
    Write-Error "It's problem to find user for forward" -ErrorAction Stop
  } 
}


Write-Host "Done!" -ForegroundColor Green